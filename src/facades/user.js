'use strict';
class UserFacade {
  constructor({ userDao }) {
    this.userDao = userDao;
  }

  getDiscountForUserById(userId) {
    let user = this.userDao.getUserById(userId);

    return user.calculateDiscountForUser(user);
  }
}

module.exports = UserFacade;