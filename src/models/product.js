'use strict';

class Product {
  constructor(data = {}) {
    this.id = data.id;
    this.productName = data.productName;
    this.productDescription = data.productDescription;
    this.productPrice = data.productPrice;
    this.categoryName = data.categoryName;
    this.productRating = data.productRating;
    this.productReviews = data.productReviews;
  }

  getId() {
    return this.id;
  }

  setId(id) {
    this.id = id;
  }

  getProductName() {
    return this.productName;
  }

  setProductName(productName) {
    this.productName = productName;
  }

  getProductDescription() {
    return this.productDescription;
  }

  setProductDescription(productDescription) {
    this.productDescription = productDescription;
  }

  getProductPrice() {
    return this.productPrice;
  }

  setProductPrice(productPrice) {
    this.productPrice = productPrice;
  }

  getCategoryName() {
    return this.categoryName;
  }

  setCategoryName(categoryName) {
    this.categoryName = categoryName;
  }

  getProductRating() {
    return this.productRating;
  }

  setProductRating(productRating) {
    this.productRating = productRating;
  }

  getProductReviews() {
    return this.productReviews;
  }

  setProductReviews(productReviews) {
    this.productReviews = productReviews;
  }
}

module.exports = Product;